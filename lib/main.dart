import 'package:flutter/material.dart';
import 'package:mystery_of_the_name/app.dart';

void main() {
  runApp(const App());
}